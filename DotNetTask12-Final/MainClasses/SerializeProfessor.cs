﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DotNetTask12_Final.MainClasses
{
    class SerializeProfessor
    {
        /*This function prints the professor data from the professor table to the default location inside the debug folder*/
        public static void printProfessorTableToJSON()
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                string jsonString=""; // Making a empty string to add the serialized objects
                List<Professor> allProfessors = postGradDbContext.Professors.ToList();
                foreach (var x in allProfessors)
                {
                    jsonString += JsonConvert.SerializeObject(x);
                }
                File.WriteAllText("outputJson", jsonString); // Writes to default output folder, the professor objects in JSON form
            }
        }
    }
}
