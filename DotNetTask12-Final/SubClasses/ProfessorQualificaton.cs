﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask12_Final
{
    /*This class is responsiblke for binding the professor entity with the qualification entity, many to many.*/
    class ProfessorQualificaton
    {public int ProfessorId { get; set; }
        public Professor Professor { get; set; }
        public int QualificationId { get; set; }
        public Qualification Qualification { get; set; }
    }
}
