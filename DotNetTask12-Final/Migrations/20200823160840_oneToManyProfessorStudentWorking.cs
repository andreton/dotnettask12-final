﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DotNetTask12_Final.Migrations
{
    public partial class oneToManyProfessorStudentWorking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Students_ProfessorId",
                table: "Students");

            migrationBuilder.CreateIndex(
                name: "IX_Students_ProfessorId",
                table: "Students",
                column: "ProfessorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Students_ProfessorId",
                table: "Students");

            migrationBuilder.CreateIndex(
                name: "IX_Students_ProfessorId",
                table: "Students",
                column: "ProfessorId",
                unique: true);
        }
    }
}
