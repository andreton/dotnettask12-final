﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask12_Final
{
    /*QUalification is in a relationship with professor and is bound bu ProfessorQualification. Qualification and professor has a many to many relationship*/
    class Qualification
    { 
        public int Id { get; set; }
        public DateTime AwardedDate { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public ICollection<ProfessorQualificaton>  professorQualification { get; set; }
    }
}
