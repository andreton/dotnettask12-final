﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask12_Final
{
    class ResearchProject
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public  DateTime StartDate { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }
    }
}
