﻿using DotNetTask12_Final.MainClasses;
using System;
using System.Collections.Generic;

namespace DotNetTask12_Final
{
    class Program
    {
        static void Main(string[] args)
        {
            /*In the main function you get 6 different options, a boolean variable is continuisly running the program until user press (6)*/
            string input;
            Boolean state = true;
            while (state)
            {

                Console.WriteLine("\nWelcome to this student/professor database: Please choose one of the following: \n" +
                    "List students(1)\nList professors(2)\nAdd a professor or student (3)\nDelete a professor or student(4)\nUpdate a student or professor (5)\nPrint professordata to JSON(6)\nexit application (7)");
                string userOption = Console.ReadLine();
               
                switch (userOption)
                {
                    /*Case 1 gives the user the option to list all or use a query based on the name of the student*/
                    case "1":
                        Console.WriteLine("Do you want to list all students(1) or use a custom query (2)");
                        string listAllOrQuery = Console.ReadLine();
                        if (listAllOrQuery == "1")
                        {
                            Student.readAllStudent();
                        }
                        else if (listAllOrQuery == "2")
                        {
                            Console.WriteLine("Enter the full name or parts of the name of the student");
                            string studentName = Console.ReadLine();
                            Student.queryStudent(studentName);
                        }
                        break;
                        /*Case 2 gives the user the option to list all or use a query based on name or subject*/
                    case "2":
                        Console.WriteLine("Do you want to list all professors (1) or use a custom query (2)");
                        string listAllOrQueryProfessor = Console.ReadLine();
                        if (listAllOrQueryProfessor == "1")
                        {
                            Professor.readAllProfessors();
                        }
                        else if (listAllOrQueryProfessor == "2")
                        {
                            Console.WriteLine("Do you want to search based on name(1)  or subject(2) ");
                            string nameOrSubject = Console.ReadLine();
                            Console.WriteLine("Enter search word");
                            string searchWord = Console.ReadLine();
                            /*Calling the same query function with different parameters*/
                            if (nameOrSubject == "1")
                            {
                                Professor.queryProfessor(searchWord,"name");
                            }
                            else if (nameOrSubject == "2")
                            {
                                Professor.queryProfessor(searchWord, "subject");
                            }
                        }
                        break;
                    case "3":
                    /*Does the user want to enter a stduent or a professor*/
                        Console.WriteLine("Do you wish to add a student(1) or professor(2)");
                        input = Console.ReadLine();
                        if (input == "1")
                        {
                            Console.WriteLine("Enter first name: ");
                            string studentName = Console.ReadLine();
                            Console.WriteLine("Enter Last name: ");
                            string studentLastName = Console.ReadLine();
                            Console.WriteLine("Enter Age: ");
                            string studentAge = Console.ReadLine();
                            Console.WriteLine("Does the student have a current professor(c), or do you want to make a new professor(n)?");
                            string professorStatus = Console.ReadLine();
                            /*In order to add a student you need to bind it to a professor, this is done with either creating a new or search for the subject the professor teaches */
                            if (professorStatus == "n")
                            {
                                Console.WriteLine("Enter the professor first name: ");
                                string profName = Console.ReadLine();
                                Console.WriteLine("Enter the professor Last name: ");
                                string profLastName = Console.ReadLine();
                                Console.WriteLine("Enter the professor's Subject: ");
                                string subjectName = Console.ReadLine();
                                Professor argumentProfessor = new Professor() { FirstName = profName, LastName = profLastName, Subject = subjectName };
                                Student argumentStudent = new Student() { FirstName = studentName, LastName = studentLastName, Age = Int32.Parse(studentAge), Professor = argumentProfessor };
                                Student.AddNewStudent(argumentStudent);
                                Console.WriteLine("Student is added");
                               
                            }
                            else if (professorStatus == "c")
                            {
                                Console.WriteLine("Enter the subject the professor teaches in: ");
                                string professorSubject = Console.ReadLine();
                                try
                                {
                                    Student argumentStudent = new Student() { FirstName = studentName, LastName = studentLastName, Age = Int32.Parse(studentAge), ProfessorId = Professor.findProfessorId(professorSubject, "Subject") };
                                    Student.AddNewStudent(argumentStudent);
                                    Console.WriteLine("Student is added");
                                }
                                /*Need to catch an exception here, as the find professor ID returns -1 if not finding anything*/
                                catch(Exception e)
                                {
                                    Console.WriteLine("Something went wrong, please try again");
                                    Console.WriteLine(e.Message);
                                }
                             
                            }
                        }
                        else if (input == "2")
                        {
                            Console.WriteLine("Enter first name: ");
                            string profName = Console.ReadLine();
                            Console.WriteLine("Enter Last name: ");
                            string profLastName = Console.ReadLine();
                            Console.WriteLine("Enter Subject name: ");
                            string subjectName = Console.ReadLine();
                            Professor argumentProfessor = new Professor() { FirstName = profName, LastName = profLastName, Subject = subjectName };
                            Professor.AddNewProfessor(argumentProfessor);
                            Console.WriteLine("Professor is added");
                        }
                        else
                        {
                            Console.WriteLine("Illegal input, please try again");
                        }
                        break;
                    case "4":
                        Console.WriteLine("Enter if you want to delete a student(s) or a professor(p):");
                        string delProfOrStud = Console.ReadLine();
                        Console.WriteLine("Enter the ID of the person you want to delete: ");
                        string profOrStudId = Console.ReadLine();
                        if (delProfOrStud == "s")
                        {
                            if (Student.checkIfStudentExists(Int32.Parse(profOrStudId))){
                                Student.removeStudent(Int32.Parse(profOrStudId));
                                Console.WriteLine("Sucsessfully deleted student");
                            }
                            else
                            {
                                break;
                            }

                        }
                        else if(delProfOrStud=="p")
                        {
                            if (Professor.CheckIfProfessorExists(Int32.Parse(profOrStudId)))
                            {
                                Professor.removeProfessor(Int32.Parse(profOrStudId));
                                Console.WriteLine("Sucsessfully deleted professor");
                            }
                        }
                        break; 
                    case "5":
                        Console.WriteLine("Do you want to update student(s) or professor(p)?");
                        string updateStudentOrProf=Console.ReadLine();
                        if (updateStudentOrProf == "s")
                        {
                            Console.WriteLine("Enter the ID of the student you want to change: ");
                            string studentInput = Console.ReadLine();
                            if (Student.checkIfStudentExists(Int32.Parse(studentInput))==true){
                                Student.updateStudent(Int32.Parse(studentInput));
                                Console.WriteLine("Success");
                            }
                            else
                            {
                                Console.WriteLine("The student does not exists");
                            }
                        }
                        else if(updateStudentOrProf=="p")
                        {
                            Console.WriteLine("Enter the ID of the professor you want to change: ");
                            string professorInput = Console.ReadLine();
                            if (Professor.CheckIfProfessorExists(Int32.Parse(professorInput)) == true)
                            {
                                Professor.updateProfessor(Int32.Parse(professorInput));
                                Console.WriteLine("Success");
                            }
                            else
                            {
                                Console.WriteLine("The professor doesn't exists");
                            }
                        }
                        break;
                    case "6":

                        SerializeProfessor.printProfessorTableToJSON();
                            break;
                    case "7":
                        Environment.Exit(0);
                        break;
                    default:
                        break;
                }
            }

        }
    }
}
