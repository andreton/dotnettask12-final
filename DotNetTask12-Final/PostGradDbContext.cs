﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask12_Final
{
    class PostGradDbContext : DbContext
    {
        /*Creating a DbSet of professor and students. The subclasses attributes are automatically added*/
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Student> Students { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=PC7372\SQLEXPRESS;Initial Catalog=dotNetTask12Final;Integrated Security=True");

        }
        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            modelbuilder.Entity<ProfessorQualificaton>().HasKey(pq => new
            {
                pq.ProfessorId,
                pq.QualificationId
            });
        }

    }
}
