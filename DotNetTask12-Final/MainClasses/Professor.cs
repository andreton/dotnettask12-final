﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DotNetTask12_Final
{
    class Professor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Subject { get; set; }
        public ICollection<Student> Student { get; set; } // By using ICollection we get a one to many relationship between professor and student
        public ICollection<ProfessorQualificaton> ProfessorQualification { get; set; }  /*Profeccor qualification is an "between 
                                                                                         entity that binds the Profeccor entity to the qualification entity 
                                                                                         The relationship between professor and qualification is many to many */
        public override string ToString()
        {
            return "Professorname:\t" + FirstName + " " + LastName + " Subject: \t" + Subject;
        }

        /*Professor are the one in the one to many entity and don't need to be assigned a new student on creation*/
        public static void AddNewProfessor(Professor professorParameter)
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                postGradDbContext.Professors.Add(professorParameter);
                postGradDbContext.SaveChanges();
            }
        }

        /*Boolean check if professor exists*/
        public static Boolean CheckIfProfessorExists(int id)
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                var checkIfEmpty = from p in postGradDbContext.Professors
                                   where p.Id.Equals(id)
                                   select p;
                if (!checkIfEmpty.Any())
                {
                    Console.WriteLine("There are no professors with that ID");
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
       /*Query on Professor based on either name or subject*/
        public static void queryProfessor(string input, string inputClass)
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                if (inputClass.ToLower() == "name") //ToLower makes sure that capital letters don't matter
                {
                    var searchSubject = from p in postGradDbContext.Professors
                                        where p.FirstName.Contains(input) || p.LastName.Contains(input)
                                        select p;
                    if (!searchSubject.Any())
                    {
                        Console.WriteLine("Found none professors");
                    }
                    else
                    {
                        foreach (var x in searchSubject)
                        {
                            Console.WriteLine(x.ToString());
                        }
                    }
                }
                else if (inputClass.ToLower() == "subject")
                {
                    var searchSubject = from p in postGradDbContext.Professors
                                        where p.Subject.Contains(input)
                                        select p;
                    if (!searchSubject.Any())
                    {
                        Console.WriteLine("Found none professors");
                    }
                    else
                    {
                        foreach(var x in searchSubject)
                        {
                            Console.WriteLine(x.ToString());
                        }
                    }
                }
                
            }
        }
        /*Function for reading all professors, simply using the ToList method*/
        public static void readAllProfessors()
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                List<Professor> allProfessors = postGradDbContext.Professors.ToList();
                foreach (var x in allProfessors)
                {
                    Console.WriteLine("Professorname:\t" + x.FirstName + " " + x.LastName + " Subject:\t" + x.Subject);
                }
            }
        }
        /*Update professor, I choosed to update all the attributes of the professor */
        public static void updateProfessor(int id)
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                var professor = postGradDbContext.Professors.Find(id);
                Console.WriteLine("Enter new name:");
                string newName = Console.ReadLine();
                Console.WriteLine("Read new last name:");
                string newLastName = Console.ReadLine();
                Console.WriteLine("Enter new Subject");
                string newSubject = Console.ReadLine();
                professor.FirstName = newName;
                professor.LastName = newLastName;
                professor.Subject = newSubject;
                postGradDbContext.SaveChanges();
            }
        }

        /*Remove professor, before this method I check if the ID exists with the method: CheckIfprofessorExists */
        public static void removeProfessor(int Id)
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                var professor = postGradDbContext.Professors.Find(Id);
                postGradDbContext.Professors.Remove(professor);
                postGradDbContext.SaveChanges();
            }
        }
        /*Finding the ID of the professor, return -1 if it not exists*/
        public static int findProfessorId(string input, string inputClass) 
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                if (inputClass.ToLower() == "name")
                {
                    var searchSubject = from s in postGradDbContext.Professors
                                        where s.FirstName.Contains(input) || s.LastName.Contains(input)
                                        select s.Id;
                    return searchSubject.First();
                }
                else if (inputClass.ToLower() == "subject")
                {
                    var searchSubject = from s in postGradDbContext.Professors
                                        where s.Subject.Contains(input)
                                        select s.Id;

                    return searchSubject.First();
                }
                else
                {
                    Console.WriteLine("Did not find any professors with that Id, please start over");
                    return -1; // returning an value that cant be used for finding a value
                }
            }
        }

    }
    
}
