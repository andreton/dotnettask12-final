﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace DotNetTask12_Final
{
    class Student
    {
       
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        public int ProfessorId { get; set; }
        public Professor Professor { get; set; }// Adds one to many relationship from student to professor
    
        public ResearchProject Project { get; set; } // Adds a one to one relationship between student and researchproject

        public override string ToString()
        {
            return "Studentname:\t" + FirstName + " " + LastName + " Age: \t" + Age;

        }

        /*Function for checking if stuednt exists*/
        public static Boolean checkIfStudentExists(int id)
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                var checkIfEmpty = from s in postGradDbContext.Students
                                   where s.Id.Equals(id)
                                   select s;
                if (!checkIfEmpty.Any())
                {
                    Console.WriteLine("There are no students with that ID");
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        /*Returns student Id as an integer*/
        public static int findStudentId(string input, string inputClass)
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                if (inputClass.ToLower() == "name")
                {
                    var searchSubject = from s in postGradDbContext.Students
                                        where s.FirstName.Contains(input) || s.LastName.Contains(input)
                                        select s.Id;
                    return searchSubject.First();
                }
                else if (inputClass.ToLower() == "age")
                {
                    var searchSubject = from s in postGradDbContext.Students
                                        where s.Age.Equals(Int32.Parse(input))
                                        select s.Id;
                    return searchSubject.First();
                }
                else
                {
                    return -1;
                }
            }
        }

        /*Adds a new student*/
        public static void AddNewStudent(Student studentParameter)
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                postGradDbContext.Students.Add(studentParameter);
                postGradDbContext.SaveChanges();
            }
        }

        public static void readAllStudent()
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                List<Student> allStudents = postGradDbContext.Students.ToList();
                foreach (var x in allStudents)
                {
                    Console.WriteLine("Studentname:\t" + x.FirstName + " " + x.LastName + " Age:\t" + x.Age);
                }
            }
        }
        /*The method for query on student is only based on first or last name */
        public static void queryStudent(string searchInput)
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                List<Student> students = postGradDbContext.Students.ToList();
                    students = students.Where(x => x.FirstName.Contains(searchInput)||x.LastName.Contains(searchInput)).ToList();
                if (!students.Any())
                {
                    Console.WriteLine("We did not found any students with the specified name");
                }
                foreach (var x in students)
                {
     
                    Console.WriteLine("Studentname: " + x.FirstName + " " + x.LastName + " Age: " + x.Age);
                }
            }
        }
        /*Resets all parameters of the studetn */
        public static void updateStudent(int id)
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                var student = postGradDbContext.Students.Find(id);
                Console.WriteLine("Enter new name:");
                string newName = Console.ReadLine();
                Console.WriteLine("Read new last name:");
                string newLastName = Console.ReadLine();
                Console.WriteLine("Enter new age");
                string newAge = Console.ReadLine();

                student.FirstName = newName;
                student.LastName = newLastName;
                student.Age = Int32.Parse(newAge);
                postGradDbContext.SaveChanges();
            }
        }
        /*Remove student, if student exists */
        public static void removeStudent(int Id)
        {
            using (PostGradDbContext postGradDbContext = new PostGradDbContext())
            {
                var student = postGradDbContext.Students.Find(Id);
                postGradDbContext.Students.Remove(student);
                postGradDbContext.SaveChanges();
            }
        }
    }
}
